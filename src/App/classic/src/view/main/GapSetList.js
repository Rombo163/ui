Ext.define('App.view.main.GapSetList',{
	extend: 'Ext.grid.Panel',
	title: 'GapSet list',
	xtype: 'gapsetlist',
	store: 'dateGapSetStore',
	requires: ['App.store.DateGapSets','App.model.DateGapSet', 'App.model.Value'],
	
	columns: [
		{
			text: 'Name',
			flex: 1,
			dataIndex: 'Name' 
		},{
			text: 'Captions',
			flex: 1,
			dataIndex: 'id',
			renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
				if(record.getCaptions && record.getCaptions().getCount() !== 0) {
					var captions = [];
					var totalCaptions = record.getCaptions().getCount();
					for(var i= 0; i < totalCaptions; i++)	{
						captions.push(record.getCaptions().getAt(i).get('Value'));
						captions.push(record.getCaptions().getAt(i).get('Lang'));
					};				
					return captions;
				} else {
					return '---------------------------------------------------';
				}	
			}
		},{
			text: 'DateGapSets',
			flex: 1,
			dataIndex: 'id',
			renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
				if(record.getDateGaps() && record.getDateGaps().getCount() !== 0) {
					var dateGaps = [];
					var totalGaps = record.getDateGaps().getCount();
					for(var i = 0; i < totalGaps; i++) {
						dateGaps.push(record.getDateGaps().getAt(i).get('Name'));
						dateGaps.push(record.getDateGaps().getAt(i).get('Period'));
					};
					return dateGaps;
				} else {
					return '---------------------------------------------------';
				}			
			
			}	
		}	
	],
		tools: [{
			type: 'plus'	
		}
	]
});
