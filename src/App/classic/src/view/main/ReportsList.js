Ext.define('App.view.main.ReportsList',{
	extend: 'Ext.grid.Panel',
	title: 'Reports list',
	//alias: 'widjet.reportsList',
	xtype: 'reportslist',
	store: 'reportsStore',
	requires: ['App.store.ReportRecords','App.model.ReportRecord', 'App.model.Value'],
	
	columns: [
		{
			text: 'Name',
			flex: 1,
			dataIndex: 'Name' 
		},{
			text: 'Caption',
			flex: 1,
			dataIndex: 'id',
			renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
				if(record.getCaptions &&  record.getCaptions().getCount() !== 0) {
					console.log(record.getCaptions());					
					return record.getCaptions().getAt(0).get('Value');
				} else {
					return '---------------------------------------------------';
				}	
			}
		},{
			text: 'Comment',
			flex: 1,
			dataIndex: 'id',
			renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
				if(record.getComments && record.getComments().getCount() !== 0) {
					return record.getComments().getAt(0).get('Value');
				} else {
					return '---------------------------------------------------';
				}			
			
			}	
		}	
	],
		tools: [{
			type: 'plus'	
		}
	]
});
