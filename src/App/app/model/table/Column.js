Ext.define('App.model.table.Column', {
	requires: ['Ext.data.reader.Xml', 'Ext.data.soap.Reader', 'App.model.table.Total','App.model.Value'],
	extend: 'Ext.data.Model',
	fields: [
		{name: 'Name', mapping: 'Name', type: 'string'},
		{name: 'Alias', mapping: 'Alias', type: 'string'},
		{name: 'IsDateGapColumn', mapping: 'IsDateGapColumn', type: 'boolean'},
		{name: 'IsSplitForDateGap', mapping: 'IsSplitForDateGap', type: 'boolean'},
		{name: 'IsInternal', mapping: 'IsInternal', type: 'boolean'},
		{name: 'MasterColumnName', mapping: 'MasterColumnName', type: 'string'},
		{name: 'DataType', mapping: 'DataType', type: 'string'},
		{name: 'DbName', mapping: 'DbName', type: 'string'},
		{name: 'StringColumn', mapping: 'StringColumn', type: 'string'},
		{name: 'GroupingExpression', mapping: 'GroupingExpression', type: 'string'},
		{name: 'GroupTitleExpression', mapping: 'GroupTitleExpression', type: 'string'},
		{name: 'NullText', mapping: 'NullText', type: 'string'}	
	],
	hasMany: [
		{model: 'App.model.Value', name: 'getCaptions', associationKey: 'Caption'},
		{model: 'App.model.Value', name: 'getComments', associationKey: 'Comment'},
		{model: 'App.model.table.Total', name: 'getTotals', associationKey: 'Totals'}
	],
	proxy: {
		type: 'memory',
		reader: {
			type: 'xml',
			record: 'Column',
			rootProperty: '/'
		}
	}
});
