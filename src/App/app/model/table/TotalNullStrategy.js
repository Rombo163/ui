Ext.define('App.model.table.TotalNullStrategy', {
	xtype: 'totalnullstrat',
	statics: {
		DataTypes: {
			IGNORE: 'IGNORE',
			REPLACE: 'REPLACE',
			EMPTY: 'EMPTY'
		}
	}
});
