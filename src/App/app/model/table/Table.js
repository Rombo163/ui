Ext.define('App.model.table.Table', {
	requires: ['Ext.data.reader.Xml', 'Ext.data.soap.Reader', 'App.model.Value', 'App.model.table.Column'],
	extends: 'Extdata.Model',
	fields: [
		{name: 'name', mapping: 'Name', type: 'string'}
	],
	hasMany: [
		{model: 'App.model.Value', name: 'getCaptions', associationKey: 'Caption'},
		{model: 'App.model.Value', name: 'getComments', associationKey: 'Comment'},
		{model: 'App.model.Table.Column', name: 'getColumns', associationKey: 'Columns'},
		{model: 'App.model.Table.Filter', name: 'getFilters', associationKey: 'Filters'}
	],
	proxy: {
		type: 'memory',
		reader: {
			type: 'xml',
			record: 'Table',
			rootProperty: '/'		
		}
	}


});
