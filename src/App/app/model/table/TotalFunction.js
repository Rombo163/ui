Ext.define('App.model.table.TotalFunction', {
	xtype: 'totalfunc',
	statics: {
		TotalFunction: {
			SUM: 'SUM',
			MIN: 'MIN',
			MAX: 'MAX',
			AVG: 'AVG',
			SINGLE: 'SINGLE',
			EXPRESSION: 'EXPRESSION'
		}
	}
});
