Ext.define('App.model.ReportMetadata', {
	requires: [
		'Ext.data.reader.Xml',
		'Ext.data.soap.Reader'
	],
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'MetadataString',
			mapping: '/',			
			type: 'string'
		}
	]
});
