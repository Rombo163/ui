Ext.define('App.model.DataType', {
	xtype: 'dataenum',
	statics: {
		DataTypes: {
			STR: 'STR',
			INT: 'INT',
			SHR: 'SHR',
			LNG: 'LNG',
			DEC: 'DEC',
			DBL: 'DBL',
			DAT: 'DAT',
			DTM: 'DTM',
			BOL: 'BOL'
		}
	}
});
