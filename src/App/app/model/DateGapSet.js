Ext.define('App.model.DateGapSet',{
	requires: ['Ext.data.reader.Xml', 'Ext.data.soap.Reader'  ,'App.model.Value', 'App.model.DateGap'],
	extend: 'Ext.data.Model',
	//xtype: 'dategapset',
	autoload: true,
	fields: [
		{
			name: 'Name',
			mapping: '@name',
			type: 'string'
		}, {
			name: 'Type',
			mapping: '@type',
			type: 'string'
		}
	],
	hasMany: [
		{
			model: 'App.model.Value',
			name: 'getCaptions',
			associationKey: 'Caption'			
		}, {
			model: 'App.model.DateGap',
			name: 'getDateGaps',
			associationKey: 'DateGaps'
		}	
	]
});
