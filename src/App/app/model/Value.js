Ext.define('App.model.Value', {
	requires: ['Ext.data.reader.Xml', 'Ext.data.soap.Reader'],
	extend: 'Ext.data.Model',
	xtype: 'value',
	autoload: true,
	fields: [
		{
			name: 'Value',
			mapping: '/', 
			type: 'string'
		},{
			name: 'Lang',
			mapping: '@lang',
			type: 'string'
		}
	],
	proxy: {
		type: 'memory',
		reader: {
			type: 'xml',
			record: 'Value',
			rootProperty: '/'
		}
	}
});
