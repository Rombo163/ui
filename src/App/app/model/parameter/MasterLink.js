Ext.define('App.model.parameter.MasterLink', {
	requires: ['Ext.data.reader.Xml'],
	extend: 'Ext.data.Model',
	fields: [
		{name: 'Report', mapping: '@report', type: 'string'},
		{name: 'Type', mapping: '@type', type: 'string'},
		{name: 'Name', mapping: '@name', type: 'string'}	
	],
	proxy: {
		type: 'memory',
		reader: {
			type: 'xml',
			record: 'MasterLink',
			rootProperty: '/'
		}
	}
});
