/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('App.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

	requires: [
		'App.model.*',
		'App.store.*',
		'Ext.data.soap.Proxy',
		'Ext.data.soap.Reader'
	],

	//store: 'reportsStore',

    onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },
    
    onClickButton: function () {
        // Remove the localStorage key/value
        localStorage.removeItem('TutorialLoggedIn');

        // Remove Main View
        this.getView().destroy();

        // Add the Login Window
        Ext.create({
            xtype: 'login'
        });
    },

	onClickButton3: function () {
		var metaStore = Ext.getStore('metaStore');
		metaStore.load({
			params: {
				ReportName: 'PL_TradeCashFlowTill',
				IncludeSlaves: true
			},
			callback: function(req,r) {
				console.log('>>>META store -> items quantity:');
				console.log(metaStore);
				console.log(metaStore.getCount());
				console.log(metaStore.getAt(0).get('MetadataString'));
				//строку CDATA -> xml n
				var xmlString = metaStore.getAt(0).get('MetadataString');
		//		var xmlString = '<rs:Report><Name>xyu\n</Name></rs:Report>';
				var xmlDocument;
				if (window.DOMParser) {
					 xmlDocument = (new DOMParser()).parseFromString(xmlString, 'text/xml');
				} else {
					 xmlDocument = new ActiveXObject("Microsoft.XMLDOM");
					 xmlDocument.async = false;
					 xmlDocument.loadXML(xmlString);
				}
				console.log(xmlDocument);
				var reportMetaStore = Ext.create('Ext.data.Store', {
					data: xmlString,
					autoload: true,
					model: 'App.model.Report',
					proxy: {
						type: 'memory',
						reader: {
							type: 'xml',
							record: 'rs|Report',
							namespace: ''			
						}
					}
				});
				reportMetaStore.load({
					callback: function(req, r) {
						console.log('>>>REPORT_META store -> items quantity:');
						console.log(reportMetaStore);
						console.log(reportMetaStore.getCount());
						console.log(reportMetaStore.getAt(0).get('Name'));
					}	
				});				
			}	
		});
	},
	
	onClickButton4: function () {
		var gapSetStore = Ext.getStore('dateGapSetStore');
		gapSetStore.load({
			callback: function (req, r) {
				console.log('>>>DateGapSet store -> items quantity:');
				console.log(gapSetStore.getCount());
				console.log('>>>DateGapSet store -> first item:'); 
				console.log(gapSetStore.getAt(0));
				console.log(gapSetStore.getAt(0).get('Name'));
				console.log('>>>Captions store');
				console.log(gapSetStore.getAt(0).getCaptions());
				console.log('>>>Captions store -> items total:');
				console.log(gapSetStore.getAt(0).getCaptions().getCount());
				console.log(gapSetStore.getAt(0).getCaptions().getAt(0));
				console.log('>>>DateGaps store');
				console.log(gapSetStore.getAt(0).getDateGaps());
				console.log('>>>DateGaps store -> items total:')				
				console.log(gapSetStore.getAt(0).getDateGaps().getCount());
				console.log('>>>DateGaps store -> first item:');				
				console.log(gapSetStore.getAt(0).getDateGaps().getAt(0));
			//	console.log(gapSetStore.getAt(0).getDateGaps().getAt(0).get('Name'));
			//	console.log(gapSetStore.getAt(0).getDateGaps().getAt(0).get('Period'));
			//	console.log(gapSetStore.getCount());
			}
		});
	},	

	onClickButton2: function () {

	var store = Ext.getStore('reportsStore');	
	store.getProxy().getReader().setKeepRawData(true);	
	store.load({
		 callback: function(req,r) {		  
				
		     console.log(store.getCount()); 
		//	  console.log(store.getAt(0).Comment);
		     
//			  var nodes = store.getProxy().getReader().rawData.querySelectorAll('Comment Value');			
//			  for(var key in store.getProxy().getReader().rawData/*.documentElement*/) {
//					console.log('name:   ' + key + '								value:  ' + store.getProxy().getReader().rawData/*.documentElement*/[key]);			
//		  };	
			var reportNodes = store.getProxy().getReader().rawData.querySelectorAll('ReportSummary');	
/////////////////////////////////////////////////////////////////////////////////////////				
			var inFor = store.getAt(0);
			console.log(inFor);
			console.log(store.getAt(0));
			console.log('>>>Comments store')
			console.log(store.getAt(0).getComments());
			console.log('>>>Comments store, first item');
			console.log(store.getAt(0).getComments().getAt(0));
			console.log(store.getAt(0).getComments().getAt(0).get('Value'));
		//	console.log('>>>Captions store')
		//	console.log(store.getAt(0).getCaptions());
		//	console.log('>>>Captions store, first item')
		//	console.log(store.getAt(0).getCaptions().getAt(0));

		//	for(var key in inFor) {
		//			console.log('name:   ' + key + '						value:  ' + inFor[key]);
		//   };
//////////////////////////////////////////////////////////////////////////////////////////
			var report = reportNodes[0];
			console.log(report.querySelectorAll('Value') + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
			console.log(report.querySelectorAll('Value')[0].textContent + '>>>>>>>>>>>>>>>>>>>>>>>>>>');
			
			var values = report.querySelectorAll('Value'); 			

//			for(var key in report) {
//					console.log('name:   ' + key + '						value:  ' + report[key]);			
//		   };	
			console.log('-----------------------------------------');
			var dataType = Ext.create('App.model.DataType');  
				
			console.log(App.model.DataType.DataTypes.INT); 
			console.log(App.model.DataType.DataTypes.LNG);   
			console.log('-----------------------------------------');
			
			  for(var key in store.getAt(0).data) {
					console.log('name:   ' + key + '						value:  ' + store.getAt(0).data[key]);			
		   };	

			
//			var rawDataString;
//			rawDataString = store.getProxy().getReader().rawData.documentElement.innerHTML;
//			console.log(rawDataString); 
		} 
	});			
   }
});
